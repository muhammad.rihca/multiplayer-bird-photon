﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawn : MonoBehaviour
{
    public GameObject coinPrefabs;
    int totalSpawnCoin, oldTotalSpawnCoin;

    // Start is called before the first frame update
    void Start()
    {

        totalSpawnCoin = Random.Range(4, 10);
        oldTotalSpawnCoin = totalSpawnCoin;
        for (int i = 1; i < totalSpawnCoin; i -= -1)
        {
            Debug.Log(totalSpawnCoin);
            Vector3 coinPrefabsPos = new Vector3(Random.Range(-8, 8), Random.Range(-4, 4), 0f);
            Instantiate(coinPrefabs, coinPrefabsPos, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnCoin()
    {
        if (oldTotalSpawnCoin < totalSpawnCoin)
        {
            bool coinSpawned = false;
            while (!coinSpawned)
            {
                Vector3 coinPrefabsPos = new Vector3(Random.Range(-8, 8), Random.Range(-4, 4), 0f);
                if ((coinPrefabsPos - transform.position).magnitude < 3)
                {
                    continue;
                }
                else
                {
                    Instantiate(coinPrefabs, coinPrefabsPos, Quaternion.identity);
                    coinSpawned = true;
                }
            }
        }
    }

    public void DecreaseOldTotalSpawn()
    {
        oldTotalSpawnCoin += -1;
        Invoke("SpawnCoin", 3);
    }
}
