﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public CoinManager coinManager;
    public Text textHighScore;
    public GameObject gameOver;

    float currentTime;
    public float startingTime = 10f;

    [SerializeField] Text countdownText;

    void Awake()
    {
        Time.timeScale = 1;
    }
    // Start is called before the first frame update
    void Start()
    {
        currentTime = startingTime;
        if (PlayerPrefs.HasKey("HighScore"))
        {
            textHighScore.text = "Hi Score: " + PlayerPrefs.GetInt("HighScore").ToString();
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        countdownText.text = currentTime.ToString("0");

        if (currentTime <= 0)
        {
            if (PlayerPrefs.HasKey("HighScore"))
            {
                CekHighScore();
            }

            currentTime = 0;
            Time.timeScale = 0;
            gameOver.SetActive(true);
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    void CekHighScore(){
        if (coinManager.totalCoin > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", coinManager.totalCoin);
        }
    }
}
