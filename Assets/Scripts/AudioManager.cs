﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance = null;
    public static AudioManager Instance{
        get{
            if (_instance  == null)
            {
                _instance = FindObjectOfType<AudioManager>();
            }
            return _instance;
        }
    }
    [SerializeField]
    AudioSource audioSource;
    [SerializeField]
    List<AudioClip> audioClip;

    public void Play(string name){
        AudioClip sound = audioClip.Find(s=>s.name == name);
        if(sound == null){
            return;
        }
        audioSource.PlayOneShot(sound);
    }
}
