﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Player : MonoBehaviour
{
    PhotonView view;
    public CoinManager coinManager;
    public CoinSpawn coinSpawn;
    Camera cam;
    private Rigidbody2D rigidBody2D;
    private float xInitialForce = 5.0f, yInitialForce = 5.0f;

    public KeyCode upButton = KeyCode.W;
    public KeyCode downButton = KeyCode.S;
    public KeyCode leftButton = KeyCode.A;
    public KeyCode rightButton = KeyCode.D;

    public float speed = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();

        rigidBody2D = GetComponent<Rigidbody2D>();
        // Invoke("PushBall", 0);
        // PushBall();
    }

    // Update is called once per frame
    void Update()
    {
        if (view.IsMine)
        {
            Vector2 velocity = rigidBody2D.velocity;
            if (Input.GetKey(upButton))
            {
                velocity.y = speed;
            }
            else if (Input.GetKey(downButton))
            {
                velocity.y = -speed;
            }
            else
            {
                velocity.y = 0.0f;
            }

            if (Input.GetKey(leftButton))
            {
                velocity.x = -speed;
            }
            else if (Input.GetKey(rightButton))
            {
                velocity.x = speed;
            }
            else
            {
                velocity.x = 0.0f;
            }
            rigidBody2D.velocity = velocity;

            if (Input.GetMouseButton(0))
            {
                Vector2 cursorPos = cam.ScreenToWorldPoint(Input.mousePosition);
                transform.position = new Vector2(cursorPos.x, cursorPos.y);
            }
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Coin")
        {
            AudioManager.Instance.Play("Coin");
            Destroy(collision.gameObject);
            coinManager.IncrementCoin();
            coinSpawn.DecreaseOldTotalSpawn();
        }
    }
}
