﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinManager : MonoBehaviour
{
    
    public Text textCoin;
    public int totalCoin;

    // Start is called before the first frame update
    void Start()
    {
        totalCoin = 0;
    }

    // Update is called once per frame
    void Update()
    {
        textCoin.text = "Score: " + totalCoin.ToString();
    }

    public void IncrementCoin(){
        totalCoin -= -1;
    }
}
